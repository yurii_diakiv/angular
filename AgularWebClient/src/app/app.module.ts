import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectModule } from "./modules/project-module/project.module";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';
import { ProjectsShowComponent } from './modules/project-module/projects-show/projects-show.component';
import { ProjectAddComponent } from './modules/project-module/project-add/project-add.component';
import { ProjectEditComponent } from './modules/project-module/project-edit/project-edit.component';
import { ProjectTaskModule } from './modules/project-task-module/project-task.module';
import { TeamModule } from './modules/team-module/team.module';
import { UserModule } from './modules/user-module/user.module';
import { ProjectTaskAddComponent } from './modules/project-task-module/project-task-add/project-task-add.component';
import { ProjectTasksShowComponent } from './modules/project-task-module/project-tasks-show/project-tasks-show.component';
import { TeamAddComponent } from './modules/team-module/team-add/team-add.component';
import { TeamEditComponent } from './modules/team-module/team-edit/team-edit.component';
import { TeamsShowComponent } from './modules/team-module/teams-show/teams-show.component';
import { UserAddComponent } from './modules/user-module/user-add/user-add.component';
import { UserEditComponent } from './modules/user-module/user-edit/user-edit.component';
import { UsersShowComponent } from './modules/user-module/users-show/users-show.component';
import { MyDatePipe } from './pipes/date.pipe';
import { ProjectTaskEditComponent } from './modules/project-task-module/project-task-edit/project-task-edit.component';



const appRoutes: Routes = [
  { path: 'projects-show', component: ProjectsShowComponent },
  { path: "project-add", component: ProjectAddComponent },
  { path: "project-edit", component: ProjectEditComponent },

  { path: "project-task-add", component: ProjectTaskAddComponent },
  { path: "project-task-edit", component: ProjectTaskEditComponent },
  { path: "project-tasks-show",component: ProjectTasksShowComponent },

  { path: "team-add", component: TeamAddComponent },
  { path: "team-edit", component: TeamEditComponent },
  { path: "teams-show", component: TeamsShowComponent },

  { path: "user-add", component: UserAddComponent},
  { path: "user-edit", component: UserEditComponent },
  { path: "users-show", component: UsersShowComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    // MyDatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProjectModule,
    ProjectTaskModule,
    TeamModule,
    UserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent],
  // exports: [
  //   MyDatePipe
  // ]
})
export class AppModule { }
