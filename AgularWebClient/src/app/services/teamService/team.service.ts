import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/team';

@Injectable(
//   {
//   providedIn: 'root'
// }
)
export class TeamService {

  constructor(private http: HttpClient) { }

  getTeams(): Observable<Team[]>
  {
    return this.http.get<Team[]>("https://localhost:44372/api/teams");
  }

  addTeam(team: Team)
  {
    return this.http.post<Team>("https://localhost:44372/api/teams", team);
  }
  
  editTeam(team: Team)
  {
    return this.http.put("https://localhost:44372/api/teams", team);
  }
}
