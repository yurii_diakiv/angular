import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProjectTask } from 'src/app/models/projectTask';

@Injectable(
  // {
  // providedIn: 'root'
// }
)
export class ProjectTaskService {

  constructor(private http: HttpClient) { }

  getProjectTasks(): Observable<ProjectTask[]>
  {
    return this.http.get<ProjectTask[]>("https://localhost:44372/api/tasks");
  }

  addProjectTask(projectTask: ProjectTask)
  {
    return this.http.post<ProjectTask>("https://localhost:44372/api/tasks", projectTask);
  }
  
  editProjectTask(projectTask: ProjectTask)
  {
    return this.http.put("https://localhost:44372/api/tasks", projectTask);
  }
}
