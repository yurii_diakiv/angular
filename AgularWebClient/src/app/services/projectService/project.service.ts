import { Injectable } from '@angular/core';
import { Project } from "../../models/project";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
// import { JsonPipe } from '@angular/common';

@Injectable(
  // {
  // providedIn: 'root'
  // }
)
export class ProjectService {

  constructor(private http: HttpClient) { }

  getProjects(): Observable<Project[]>
  {
    return this.http.get<Project[]>("https://localhost:44372/api/projects");
  }

  addProject(project: Project)
  {
    return this.http.post<Project>("https://localhost:44372/api/projects", project);
  }
  
  editProject(project: Project)
  {
    return this.http.put("https://localhost:44372/api/projects", project);
  }
}
