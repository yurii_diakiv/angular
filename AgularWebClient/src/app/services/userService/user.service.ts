import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';

@Injectable(
//   {
//   providedIn: 'root'
// }
)
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]>
  {
    return this.http.get<User[]>("https://localhost:44372/api/users");
  }

  addUser(user: User)
  {
    return this.http.post<User>("https://localhost:44372/api/users", user);
  }
  
  editUser(user: User)
  {
    return this.http.put("https://localhost:44372/api/users", user);
  }
}
