import { Pipe, PipeTransform } from "@angular/core";
import { Project } from '../models/project';

@Pipe({
    name: "MyDatePipe"
})
export class MyDatePipe implements PipeTransform{
    transform(value: Date, args?: any): any{
        return `${value.toLocaleDateString("uk")}`;
    }
}