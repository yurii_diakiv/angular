import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/projectService/project.service';
import { Project } from 'src/app/models/project';
import { map } from "rxjs/operators";
import { typeofExpr } from '@angular/compiler/src/output/output_ast';



@Component({
  selector: 'app-projects-show',
  templateUrl: './projects-show.component.html',
  styleUrls: [
    './projects-show.component.css'],
  providers: [ProjectService]
})
export class ProjectsShowComponent implements OnInit {

  projects: Project[] = [];

  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.projectService.getProjects().pipe(map(x => {
      x = x.map( p => {
        p.createdAt = new Date(p.createdAt);
        p.deadline = new Date(p.deadline);
        return p;
      })
      return x;
    })).subscribe(x => this.projects = x);
  }
}

