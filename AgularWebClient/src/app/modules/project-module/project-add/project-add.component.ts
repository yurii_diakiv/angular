import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/projectService/project.service';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css'],
  providers: [ProjectService]
})
export class ProjectAddComponent implements OnInit {

  constructor(private projectService: ProjectService) { }

  ngOnInit() {
  }

  addProject()
  {
    let name:string = (<HTMLInputElement>document.getElementById("nameInput2")).value;
    let description:string = (<HTMLInputElement>document.getElementById("descriptionInput2")).value;
    let createdAt: Date = new Date((<HTMLInputElement>document.getElementById("createdAtInput2")).value)
    let deadline:Date = new Date((<HTMLInputElement>document.getElementById("deadlineInput2")).value);
    let authorId:number = +(<HTMLInputElement>document.getElementById("authorIdInput2")).value;
    let teamId:number = +(<HTMLInputElement>document.getElementById("teamIdInput2")).value;

    let project: Project = new Project(name, description, createdAt, deadline, authorId, teamId);
    console.log(project);
    this.projectService.addProject(project).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
    window.location.reload();
  }

}
