import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProjectsShowComponent } from "../projects-show/projects-show.component";
import { ProjectService } from 'src/app/services/projectService/project.service';
import { Project } from 'src/app/models/project';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css'],
  providers: [ProjectService]
})
export class ProjectEditComponent implements OnInit {

  projects: Project[] = [];

  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.projectService.getProjects().pipe(map(x => {
      x = x.map( p => {
        p.createdAt = new Date(p.createdAt);
        p.deadline = new Date(p.deadline);
        return p;
      })
      return x;
    })).subscribe(x => this.projects = x);
  }

  editProject(id: number, name: string, description: string, createdAt: Date, deadline: Date, authotId: number, teamId: number)
  {
    let project: Project = new Project(name, description, createdAt, deadline, authotId, teamId);
    project.id = id;
    this.projectService.editProject(project).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
  }
}
