import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsShowComponent } from './projects-show/projects-show.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectAddComponent } from './project-add/project-add.component';

import { FormsModule }   from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
// import { MyDatePipe } from "../../pipes/date.pipe";

@NgModule({
  declarations: [
    ProjectsShowComponent,
    ProjectEditComponent,
    ProjectAddComponent,
    
    // MyDatePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    ProjectsShowComponent
  ]
})
export class ProjectModule { }
