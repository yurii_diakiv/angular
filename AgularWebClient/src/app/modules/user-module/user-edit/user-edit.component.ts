import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/userService/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService]
})
export class UserEditComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  editUser(id: number, firstName: string, lastName: string, email: string, birthday: Date, registredAt: Date, teamId: number)
  {
    let user: User = new User(firstName, lastName, email, birthday, registredAt, teamId);
    user.id = id;
    this.userService.editUser(user).subscribe(
      response => console.log(response),
      err => console.log(err)
    );
  }
}
