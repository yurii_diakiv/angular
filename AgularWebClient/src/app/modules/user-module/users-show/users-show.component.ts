import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/userService/user.service';
import { User } from 'src/app/models/user';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-users-show',
  templateUrl: './users-show.component.html',
  styleUrls: ['./users-show.component.css'],
  providers: [UserService]
})
export class UsersShowComponent implements OnInit {

  users: User[] = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().pipe(map(x => {
      x = x.map(t => {
        t.birthday = new Date(t.birthday);
        t.registredAt = new Date(t.registredAt);
        return t;
      })
      return x;
    })).subscribe(x => this.users = x);
  }


}
