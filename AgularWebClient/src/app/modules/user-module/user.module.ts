import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UsersShowComponent } from './users-show/users-show.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserAddComponent, UserEditComponent, UsersShowComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ]
})
export class UserModule { }
