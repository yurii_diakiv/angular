import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/userService/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css'],
  providers: [UserService]
})
export class UserAddComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  addUser()
  {
    let firstName:string = (<HTMLInputElement>document.getElementById("firstNameInput2")).value;
    let lastName:string = (<HTMLInputElement>document.getElementById("lastNameInput2")).value;
    let email: string = (<HTMLInputElement>document.getElementById("emailInput2")).value;
    let birthday: Date = new Date((<HTMLInputElement>document.getElementById("birthdayInput2")).value)
    let registredAt:Date = new Date((<HTMLInputElement>document.getElementById("registredAtInput2")).value);
    let teamId:number = +(<HTMLInputElement>document.getElementById("teamIdInput2")).value;

    let user: User = new User(firstName, lastName, email, birthday, registredAt, teamId);
    
    this.userService.addUser(user).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
    window.location.reload();
  }
}
