import { Component, OnInit } from '@angular/core';
import { ProjectTaskService } from 'src/app/services/projectTaskService/project-task.service';
import { ProjectTask } from 'src/app/models/projectTask';

@Component({
  selector: 'app-project-task-add',
  templateUrl: './project-task-add.component.html',
  styleUrls: ['./project-task-add.component.css'],
  providers : [
    ProjectTaskService
  ]
})
export class ProjectTaskAddComponent implements OnInit {

  constructor(private projectTaskService: ProjectTaskService) { }

  ngOnInit() {
  }

  addProjectTask()
  {
    let name:string = (<HTMLInputElement>document.getElementById("nameInput2")).value;
    let description:string = (<HTMLInputElement>document.getElementById("descriptionInput2")).value;
    let createdAt: Date = new Date((<HTMLInputElement>document.getElementById("createdAtInput2")).value)
    let finishedAt:Date = new Date((<HTMLInputElement>document.getElementById("finishedAtInput2")).value);
    let state: number = +(<HTMLInputElement>document.getElementById("stateInput2")).value;
    let projectId:number = +(<HTMLInputElement>document.getElementById("projectIdInput2")).value;
    let performerId:number = +(<HTMLInputElement>document.getElementById("performerIdInput2")).value;

    let projectTask: ProjectTask = new ProjectTask(name, description, createdAt, finishedAt, state, projectId, performerId);
    
    this.projectTaskService.addProjectTask(projectTask).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
    window.location.reload();
  }
}
