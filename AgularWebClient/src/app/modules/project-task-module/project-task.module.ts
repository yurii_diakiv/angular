import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectTaskAddComponent } from './project-task-add/project-task-add.component';
import { ProjectTaskEditComponent } from './project-task-edit/project-task-edit.component';
import { ProjectTasksShowComponent } from './project-tasks-show/project-tasks-show.component';

import { FormsModule }   from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    ProjectTaskAddComponent,
    ProjectTaskEditComponent,
    ProjectTasksShowComponent
     
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ]
})
export class ProjectTaskModule { }
