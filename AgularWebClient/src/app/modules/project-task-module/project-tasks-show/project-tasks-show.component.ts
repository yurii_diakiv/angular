import { Component, OnInit } from '@angular/core';
import { ProjectTaskService } from 'src/app/services/projectTaskService/project-task.service';
import { ProjectTask } from 'src/app/models/projectTask';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-project-tasks-show',
  templateUrl: './project-tasks-show.component.html',
  styleUrls: ['./project-tasks-show.component.css'],
  providers: [ProjectTaskService]
})
export class ProjectTasksShowComponent implements OnInit {

  projectTasks: ProjectTask[] = [];

  constructor(private projectTaskService: ProjectTaskService) { }

  ngOnInit() {
    this.projectTaskService.getProjectTasks().pipe(map(x => {
      x = x.map(t => {
        t.createdAt = new Date(t.createdAt);
        t.finishedAt = new Date(t.finishedAt);
        return t;
      })
      return x;
    })).subscribe(x => this.projectTasks = x);
  }

}
