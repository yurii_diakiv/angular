import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTasksShowComponent } from './project-tasks-show.component';

describe('ProjectTasksShowComponent', () => {
  let component: ProjectTasksShowComponent;
  let fixture: ComponentFixture<ProjectTasksShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectTasksShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTasksShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
