import { Component, OnInit } from '@angular/core';
import { ProjectTask } from 'src/app/models/projectTask';
import { ProjectTaskService } from 'src/app/services/projectTaskService/project-task.service';

@Component({
  selector: 'app-project-task-edit',
  templateUrl: './project-task-edit.component.html',
  styleUrls: ['./project-task-edit.component.css'],
  providers: [ProjectTaskService]
})
export class ProjectTaskEditComponent implements OnInit {

  constructor(private projectTaskService: ProjectTaskService) { }

  ngOnInit() {
  }

  editProjectTask(id: number, name: string, description: string, createdAt: Date, finishedAt: Date, state: number, projectId: number, performerId: number)
  {
    let project: ProjectTask = new ProjectTask(name, description, createdAt, finishedAt, state, projectId, performerId);
    project.id = id;
    this.projectTaskService.editProjectTask(project).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
  }

}
