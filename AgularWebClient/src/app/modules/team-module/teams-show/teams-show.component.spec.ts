import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsShowComponent } from './teams-show.component';

describe('TeamsShowComponent', () => {
  let component: TeamsShowComponent;
  let fixture: ComponentFixture<TeamsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
