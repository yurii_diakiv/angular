import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/teamService/team.service';
import { Team } from 'src/app/models/team';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-teams-show',
  templateUrl: './teams-show.component.html',
  styleUrls: ['./teams-show.component.css'],
  providers: [
    TeamService
  ]
})
export class TeamsShowComponent implements OnInit {

  teams: Team[] = [];

  constructor(private teamService: TeamService) { }

  ngOnInit() {
    this.teamService.getTeams().pipe(map(x => {
      x = x.map( t => {
        t.createdAt = new Date(t.createdAt);
        return t;
      })
      return x;
    })).subscribe(x => this.teams = x);
  }

}
