import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/teamService/team.service';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-team-add',
  templateUrl: './team-add.component.html',
  styleUrls: ['./team-add.component.css'],
  providers: [TeamService]
})
export class TeamAddComponent implements OnInit {

  constructor(private teamService: TeamService) { }

  ngOnInit() {
  }

  addTeam()
  {
    let name:string = (<HTMLInputElement>document.getElementById("nameInput2")).value;
    let createdAt: Date = new Date((<HTMLInputElement>document.getElementById("createdAtInput2")).value)

    let team: Team = new Team(name, createdAt);
    
    this.teamService.addTeam(team).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
    window.location.reload();
  }
}
