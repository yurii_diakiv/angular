import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamAddComponent } from './team-add/team-add.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamsShowComponent } from './teams-show/teams-show.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [TeamAddComponent, TeamEditComponent, TeamsShowComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ]
})
export class TeamModule { }
