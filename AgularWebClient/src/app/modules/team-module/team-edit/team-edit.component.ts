import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/teamService/team.service';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.css'],
  providers: [ TeamService ]
})
export class TeamEditComponent implements OnInit {

  constructor(private teamService: TeamService) { }

  ngOnInit() {
  }

  editTeam(id: number, name: string, createdAt: Date)
  {
    let team: Team = new Team(name, createdAt);
    team.id = id;
    this.teamService.editTeam(team).subscribe(
      responce => console.log(responce),
      err => console.log(err)
    );
  }

}
