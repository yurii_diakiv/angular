export class Team
{
    public id: number;
    public name: string;
    public createdAt: Date;

    constructor(name: string, createdAt: Date)
    {
        this.name = name;
        this.createdAt = createdAt;
    }
}