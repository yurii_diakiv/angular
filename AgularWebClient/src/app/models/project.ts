export class Project
{
    public id: number;
    public name: string;
    public description: string;
    public createdAt: Date;
    public deadline: Date;
    public authorId: number;
    public teamId: number;

    constructor(name: string, description: string, createdAt: Date, deadline: Date, authorId: number, teamId: number)
    {
        this.name = name;
        this.description = description;
        this.createdAt = createdAt;
        this.deadline = deadline;
        this.authorId = authorId;
        this.teamId = teamId;
    }
}