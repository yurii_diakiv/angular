export class User
{
    public id: number;
    public firstName: string;
    public lastName: string;
    public email: string;
    public birthday: Date;
    public registredAt: Date;
    public teamId: number;

    constructor(firstName: string, lastName: string, email: string, birthday: Date, registredAt: Date, teamId: number)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.registredAt = registredAt;
        this.teamId = teamId;
    }
}