export class ProjectTask
{
    public id: number;
    public name: string;
    public description: string;
    public createdAt: Date;
    public finishedAt: Date;
    public state: number;
    public projectId: number;
    public performerId: number;

    constructor(name: string, description: string, createdAt: Date, finishedAt: Date, state: number, projectId: number, performerId: number)
    {
        this.name = name;
        this.description = description;
        this.createdAt = createdAt;
        this.finishedAt = finishedAt;
        this.state = state;
        this.projectId = projectId;
        this.performerId = performerId;
    }
}