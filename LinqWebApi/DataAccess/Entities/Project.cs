﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }

        public Project(int id, string name, string description, DateTime createdAt, DateTime deadline, int? authorId, int? teamId)
        {
            Id = id;
            Name = name;
            Description = description;
            CreatedAt = createdAt;
            Deadline = deadline;
            AuthorId = authorId;
            TeamId = teamId;
        }
    }
}
