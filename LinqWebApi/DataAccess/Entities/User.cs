﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegistredAt { get; set; }
        public int? TeamId { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime birthday, DateTime registredAt, int? teamId)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Birthday = birthday;
            RegistredAt = registredAt;
            TeamId = teamId;
        }
    }
}
