﻿using System;
using System.Collections.Generic;
using System.Text;
using QueueServices.Interfaces;
using QueueServices.Models;
using RabbitMQ.Client;

namespace QueueServices.Services
{
    public class MessageQueue// : IMessageQueue
    {
        private readonly IConnection connection;
        public IModel Channel { get; protected set; }

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            connection = connectionFactory.CreateConnection();
            Channel = connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
            :this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);

            if(messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey, messageScopeSettings.QueueName);
            }
        }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void BindQueue(string exchangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, durable: true, exclusive: false, autoDelete: false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            connection?.Dispose();
        }
    }
}
