﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using QueueServices.Interfaces;
using QueueServices.Models;

namespace QueueServices.Services
{
    public class MessageProducerScope// : IMessageProducerScope
    {
        private readonly Lazy</*IMessageQueue*/MessageQueue> messageQueueLazy;
        private readonly Lazy</*IMessageProducer*/MessageProducer> messageProducerLazy;
        private readonly MessageScopeSettings messageScopeSettings;
        private readonly IConnectionFactory connectionFactory;

        public /*IMessageProducer*/MessageProducer MessageProducer => messageProducerLazy.Value;
        public /*IMessageQueue*/MessageQueue MessageQueue => messageQueueLazy.Value;

        public MessageProducerScope(IConnectionFactory connFactory, MessageScopeSettings messageScopeSett)
        {
            connectionFactory = connFactory;
            messageScopeSettings = messageScopeSett;
            messageQueueLazy = new Lazy</*IMessageQueue*/MessageQueue>(CreateMessageQueue);
            messageProducerLazy = new Lazy</*IMessageProducer*/MessageProducer>(CreateMessageProducer);
        }

        private /*IMessageQueue*/MessageQueue CreateMessageQueue()
        {
            return new MessageQueue(connectionFactory, messageScopeSettings);
        }

        private /*IMessageProducer*/MessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    messageScopeSettings.ExchangeType,
                    messageScopeSettings.ExchangeName,
                    messageScopeSettings.RoutingKey)
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
