﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
//using LinqWebApi.Models;
using DataAccess.Entities;
using LinqWebApi.Interfaces;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UsersService usersService;

        public UsersController(UsersService service)
        {
            usersService = service;
        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await usersService.GetUsers();
        }

        [HttpGet("{id}")]
        public async Task<User> Get(int id)
        {
            return await usersService.GetUser(id);
        }

        [HttpPost]
        public async Task Post([FromBody] User user)
        {
            await usersService.Create(user);
        }

        [HttpPut]
        public async Task Put([FromBody] User user)
        {
            await usersService.Update(user);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await usersService.Delete(id);
        }
    }
}
