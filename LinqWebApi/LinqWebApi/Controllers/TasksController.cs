﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using LinqWebApi.Models;
using LinqWebApi.Services;
using DataAccess.Entities;


namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TasksService tasksService;

        public TasksController(TasksService service)
        {
            tasksService = service;
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectTask>> Get()
        {
            return await tasksService.GetTasks();
        }

        [HttpGet("{id}")]
        public async Task<ProjectTask> Get(int id)
        {
            return await tasksService.GetTask(id);
        }

        [HttpPost]
        public async Task Post([FromBody] ProjectTask task)
        {
            await tasksService.Create(task);
        }

        [HttpPut]
        public async Task Put([FromBody] ProjectTask task)
        {
            await tasksService.Update(task);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await tasksService.Delete(id);
        }
    }
}
