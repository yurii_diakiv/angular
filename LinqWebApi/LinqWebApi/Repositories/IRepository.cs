﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqWebApi.Repositories
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetItems();
        Task<T> GetItem(int id);
        Task Create(T item);
        Task Update(T item);
        Task Delete(int id);
    }
}
