﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using LinqWebApi.Services;
using LinqWebApi.Hubs;
using RabbitMQ.Client;
using QueueServices.Interfaces;
using QueueServices.Services;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore;
using LinqWebApi.Repositories;

namespace LinqWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddScoped<ProjectsService>();
            services.AddScoped<TasksService>();
            services.AddScoped<TaskStatesService>();
            services.AddScoped<TeamsService>();
            services.AddScoped<UsersService>();

            services.AddScoped<ProjectRepository>();
            services.AddScoped<TaskRepository>();
            services.AddScoped<TaskStateRepository>();
            services.AddScoped<TeamRepository>();
            services.AddScoped<UserRepository>();

            services.AddDbContext<DataAccess.ProjectDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProductDataBase")));


            //services.AddScoped<QueueService>();
            //services.AddScoped<MessageQueue>();
            //services.AddSingleton<QueueServices.Services.ConnectionFactory>(x => new QueueServices.Services.ConnectionFactory(new Uri(Configuration.GetSection(key: "Rabbit").Value)));
            //services.AddScoped<MessageProducer>();
            //services.AddScoped<MessageProducerScope>();
            //services.AddSingleton<MessageProducerScopeFactory>();
            //services.AddScoped<MessageConsumer>();
            //services.AddScoped<MessageConsumerScope>();
            //services.AddSingleton<MessageConsumerScopeFactory>();

            services.AddCors();
            //services.AddSignalR();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
            //.AllowAnyMethod()
            //.AllowAnyHeader()
            //.AllowCredentials()
            //.WithOrigins("http://localhost:44372"));

            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<ServerHub>(path: "/linqhub");
            //});

            app.UseHttpsRedirection();
            app.UseMvc();
            
        }
    }
}
