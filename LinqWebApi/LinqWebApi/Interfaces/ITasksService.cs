﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    public interface ITasksService
    {
        Task<IEnumerable<ProjectTask>> GetTasks();
        Task<ProjectTask> GetTask(int id);
        Task Create(ProjectTask item);
        Task Update(ProjectTask item);
        Task Delete(int id);
    }
}
