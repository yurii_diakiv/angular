﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    public interface IProjectsService
    {
        Task<IEnumerable<Project>> GetProjects();
        Task<Project> GetProject(int id);
        Task Create(Project item);
        Task Update(Project item);
        Task Delete(int id);
    }
}
