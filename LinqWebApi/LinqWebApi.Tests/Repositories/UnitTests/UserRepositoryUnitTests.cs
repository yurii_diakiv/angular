﻿using System;
using System.Collections.Generic;
using System.Text;
using FakeItEasy;
using LinqWebApi.Repositories;
using DataAccess.Entities;
using NUnit.Framework;
using LinqWebApi.Services;
using System.Threading.Tasks;
using LinqWebApi.Interfaces;
using System.Linq;


namespace LinqWebApi.Tests.Repositories.UnitTests
{
    [TestFixture]
    public class UserRepositoryUnitTests
    {
        IRepository<User> fakeUserRepository;
        
        [SetUp]
        public async Task TestSetup()
        {
            fakeUserRepository = A.Fake<IRepository<User>>();

            User user = new User
                (
                    999,
                    "Yurii",
                    "Diakiv",
                    "someemail@gmai.com",
                    new DateTime(2000, 3, 15),
                    new DateTime(2015, 3, 3),
                    null
                );

            await fakeUserRepository.Create(user);
        }

        [TearDown]
        public void TestTearDown()
        {
            fakeUserRepository.Delete(999);
        }

        [Test]
        public void CreateUserTest()
        {
            A.CallTo(() => fakeUserRepository.Create(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public async Task AddUserToTeam()
        {
            User user1 = new User
                (
                    999,
                    "Yurii",
                    "Diakiv",
                    "someemail@gmai.com",
                    new DateTime(2000, 3, 15),
                    new DateTime(2015, 3, 3),
                    3
                );

            await fakeUserRepository.Update(user1);

            A.CallTo(() => fakeUserRepository.Update(A<User>._)).MustHaveHappenedOnceExactly();
        }
    }
}
