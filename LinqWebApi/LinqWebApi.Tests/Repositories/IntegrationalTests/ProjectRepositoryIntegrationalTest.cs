﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory;
using NUnit.Framework;
using DataAccess;
using DataAccess.Entities;
using LinqWebApi.Repositories;

namespace LinqWebApi.Tests.Repositories.IntegrationalTests
{
    [TestFixture]
    public class ProjectRepositoryIntegrationalTest
    {
        [Test]
        public async Task CreateProjectTest()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase("CreateProjectTest")
                .Options;

            var dbContext = new ProjectDbContext(dbOptions);
            var pRepository = new ProjectRepository(dbContext);

            Project project = new Project
                (
                    7,
                    "fggqq",
                    "asfasf",
                    new DateTime(2000, 3, 3),
                    new DateTime(2000, 4, 4),
                    4,
                    3
                );

            await pRepository.Create(project);
            List<Project> items = (await pRepository.GetItems()).ToList();
            Assert.Contains(project, items);
        }
    }
}
