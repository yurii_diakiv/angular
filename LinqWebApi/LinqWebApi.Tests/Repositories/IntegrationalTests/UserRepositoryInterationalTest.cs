﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore.InMemory;
using Microsoft.EntityFrameworkCore;
using DataAccess;
using LinqWebApi.Repositories;
using DataAccess.Entities;
using System.Threading.Tasks;

namespace LinqWebApi.Tests.Repositories.IntegrationalTests
{
    [TestFixture]
    class UserRepositoryInterationalTest
    {
        [Test]
        public async Task RemoveUserTest()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase("RemoveUserTest")
                .Options;

            var dbContext = new ProjectDbContext(dbOptions);
            var uRepository = new UserRepository(dbContext);

            User user = new User
                (
                    999,
                    "Yurii",
                    "Diakiv",
                    "someemail@gmai.com",
                    new DateTime(2000, 3, 15),
                    new DateTime(2015, 3, 3),
                    null
                );

            await uRepository.Create(user);

            Assert.Contains(user, (await uRepository.GetItems()).ToList());

            await uRepository.Delete(999);
            Assert.IsFalse((await uRepository.GetItems()).ToList().Contains(user));
        }
    }
}
