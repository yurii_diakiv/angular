﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory;
using NUnit.Framework;
using DataAccess;
using DataAccess.Entities;
using LinqWebApi.Repositories;

namespace LinqWebApi.Tests.Repositories.IntegrationalTests
{
    [TestFixture]
    class TeamRepositoryInterationalTest
    {
        [Test]
        public async Task CreateTeamTest()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase("CreateTeamTest")
                .Options;

            var dbContext = new ProjectDbContext(dbOptions);
            var tRepository = new TeamRepository(dbContext);

            Team team = new Team
                (
                    555,
                    "Some team",
                    new DateTime(2016, 4, 3)
                );

            await tRepository.Create(team);

            Assert.Contains(team, (await tRepository.GetItems()).ToList());
        }
    }
}
